﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tank_Control : MonoBehaviour {

	public int Hp;
	public float for_speed;
	public float rot_speed;
	public GameObject bullet;
	public GameObject enemy;
	public float cooldownMax = 0.5f;
	public bool InCorner;
    Material material;

    public Sensor[] sensors;
    Transform Cannon;
	float cooldown;

	// Use this for initialization
	void Start () {
		Cannon = transform.GetChild (0);
		Hp = 25;

        material = new Material(Shader.Find("Transparent/Diffuse"));

        List<Sensor> tempList = new List<Sensor>();

        sensors = new Sensor[transform.childCount - 1];

        int temp = 0;
        for(int i = 0; i < transform.childCount; i++)
        {
            Sensor sensor = transform.GetChild(i).GetComponent<Sensor>();

            if(sensor)
            {
                tempList.Add(sensor);
                temp++;
            }
        }

        sensors = tempList.ToArray();
    }

	public void MoveVertical(float Axis)
	{
		Vector3 for_movement = transform.forward * Axis;
		for_movement *= for_speed * Time.deltaTime;
		transform.localPosition += for_movement;
	}

	public void MoveHorizontal(float Axis)
	{
		float rot_movement = Axis;
		rot_movement *= rot_speed * Time.deltaTime;
		transform.Rotate (0, rot_movement, 0);
	}

	public void Shoot()
	{
        Vector3 newDir = transform.forward;
		newDir.y = 0.0f;
		Cannon.rotation = Quaternion.LookRotation (newDir);
		if (cooldown < 0.1) {
			GameObject bull = Instantiate (bullet, Cannon.transform.position + 0.4f * Cannon.transform.forward, Cannon.transform.rotation) as GameObject;
			bull.transform.forward = Cannon.transform.forward;
            bull.GetComponent<Bullet_Move>().ChangeColor(material.color);
            bull.GetComponent<Bullet_Move>().shooter = name;
			cooldown = cooldownMax;
		}
	}

	public int GetEnemyHp()
	{
		Tank_Control temp = enemy.GetComponent<Tank_Control> ();
		return temp.Hp;
	}

	public float GetEnemyDistance(Vector3 position)
	{
		return Vector3.Distance (position, enemy.transform.position);
	}

	public float GetEnemyDiffAngle(Vector3 forward)
	{
		Vector3 a = (enemy.transform.position - transform.position);
		Vector3 b = forward;

		return Mathf.Acos(Mathf.Cos (Vector3.Dot (a, b) / (Vector3.Magnitude (a) * Vector3.Magnitude (b))));
	}

	public Vector3 AfterRotate(float Axis)
	{
		Vector3 result;
		float rot_movement = Axis;
		rot_movement *= rot_speed * Time.deltaTime;
		transform.Rotate (0, rot_movement, 0);
		result = transform.forward;
		transform.Rotate (0, -rot_movement, 0);
		return result;
	}

	public Vector3 AfterMove(float Axis)
	{
		Vector3 result;
		Vector3 for_movement = transform.forward * Axis;
		for_movement *= for_speed * Time.deltaTime;
		transform.localPosition += for_movement;
		result = transform.position;
		transform.localPosition -= for_movement;
		return result;
	}

	public bool CheckVisible(Vector3 position)
	{
		RaycastHit ObstacleHit;
		return (Physics.Raycast(transform.position, position - transform.position, out ObstacleHit, Mathf.Infinity) && position != transform.position && ObstacleHit.collider.tag == enemy.tag);
	}

	// Update is called once per frame
	void Update () {
		if (cooldown > 0)
			cooldown -= 1 * Time.deltaTime;

		if (Hp < 1)
			Destroy (gameObject);
	}

    public void ChangeColor(Color newc)
    {
        Material material = new Material(Shader.Find("Transparent/Diffuse"));
        material.color = newc;
        GetComponent<Renderer>().material = material;
    }
}
