﻿using UnityEngine;
using System.Collections;

public class Bullet_Move : MonoBehaviour {

	public float for_speed = 7.5f;
    public string shooter;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 for_movement = transform.forward;
		for_movement *= for_speed * Time.deltaTime;
		transform.localPosition += for_movement;
	}

	void OnTriggerEnter(Collider other) {
        Tank_Control tank_Control = other.gameObject.GetComponent<Tank_Control>();

        if (tank_Control)
        {
            tank_Control.Hp -= 1;
            Destroy(gameObject);
        }
        else if (other.tag == "Wall")
            Destroy(gameObject);

           
	}

    public void ChangeColor(Color newc)
    {
        Material material = new Material(Shader.Find("Transparent/Diffuse"));
        material.color = newc;
        GetComponent<Renderer>().material = material;
    }
}
