﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimMarker : MonoBehaviour {

    public Player_Control player_Control;
    public Network_Control network_Control;

    Renderer rend;
    Vector2 mouseposition;

	// Use this for initialization
	void Start () {
        ChangeColor(Color.red);
    }
	
	// Update is called once per frame
	void Update () {


        Vector3 v3 = new Vector3(mouseposition.x, mouseposition.y, 7f);
        v3 = Camera.main.ScreenToWorldPoint(mouseposition);

        transform.position = new Vector3(v3.x, v3.y, v3.z);
    }

    void ChangeColor(Color newc)
    {
        Material material = new Material(Shader.Find("Transparent/Diffuse"));
        material.color = newc;
        GetComponent<Renderer>().material = material;
    }
}
