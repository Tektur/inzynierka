﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NetworkConfigTabs
{
    ChangeNetworkLayers,
    ValidateData
}

[System.Serializable]
public class TestJson
{
    [SerializeField]
    public List<float> testList;

    public TestJson()
    {
        testList = new List<float>();
    }
}

