﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DataGather_Control : MonoBehaviour {

    static int inputNum = 8;
    static int outputNum = 3;

    [SerializeField]
    public ControlData data;

    public float timeBetween;

    public Tank_Control playerTank;
    public Tank_Control enemyTank;

    float timer;

    // Use this for initialization
    void Start () {
        timer = 0;

        if (data.inputData == null)
            data.inputData = new List<ArrayWrapper>();

        if (data.outputData == null)
            data.outputData = new List<ArrayWrapper>();

        Debug.Log("Start: " + data.inputData.Count);
	}
	
	// Update is called once per frame
	void Update () {

        timer += Time.deltaTime;

        if (timer > timeBetween)
        {
            CollectData(Input.GetButton("Jump"));
            timer = 0f;
        }
       
    }

    public void CollectData(bool shoot)
    {

        ArrayWrapper newInputData = new ArrayWrapper(inputNum);
        ArrayWrapper newOutputData = new ArrayWrapper(outputNum);

        float temp = new float();

        for (int i = 0; i < playerTank.sensors.Length; i++)
        {
            newInputData.Add(playerTank.sensors[i].inSight);
        }

        newInputData.Add(temp);

//Output

        newOutputData.Add(Input.GetAxis("Vertical"));
        newOutputData.Add(Input.GetAxis("Horizontal"));

        temp = -1f;
        if (shoot)
        {
            temp = 1f;
        }

        newOutputData.Add(temp);
        

        data.inputData.Add(newInputData);
        data.outputData.Add(newOutputData);

        Debug.Log("In game: " + data.inputData.Count + ", and temp is " + temp);
    }
}
