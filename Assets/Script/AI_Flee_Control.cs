﻿using UnityEngine;
using System.Collections;

public class AI_Flee_Control : MonoBehaviour
{

    public float timerUntilDecisionMin;
    public float timerUntilDecisionMax;
    public float speedMult;
    public float corner;

    bool inCorner;
    bool goingForward;

    Tank_Control tank_control;

    float horizontal;
    float vertical;

    float timerUntilDecision;
    float timer;
    
    public void GoForward()
    {
        horizontal = 0;
        tank_control.for_speed /= speedMult;
    }

    public void TakeATurn()
    {
        if (Random.Range(0f, 1f) > 0.5f)
            horizontal = 1;
        else
            horizontal = -1;

        tank_control.for_speed *= speedMult;
    }

    // Use this for initialization
    void Start()
    {
        vertical = 1;
        tank_control = GetComponent<Tank_Control>();
        tank_control.enemy = GameObject.FindWithTag("Player");

        tank_control.ChangeColor(Color.blue);

        timerUntilDecision = 0f;
        goingForward = true;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if(timer > timerUntilDecision)
        {
            if(goingForward)
            {
                TakeATurn();
            }
            else
            {
                GoForward();
            }

            goingForward = !goingForward;
            timer = 0;
            timerUntilDecision = Random.Range(timerUntilDecisionMin, timerUntilDecisionMax);
        }

        if (((Mathf.Abs(transform.position.x) >= corner ) || (Mathf.Abs(transform.position.z) >= corner)) && !inCorner)
        {
            if (Random.Range(0f, 1f) > 0.5f)
                horizontal = 1f;
            else
                horizontal = -1f;

            vertical = 0.5f;
            inCorner = true;
        }
        else if(!((Mathf.Abs(transform.position.x) >= corner) || (Mathf.Abs(transform.position.z) >= corner)) && inCorner)
        {
            vertical = 1f;
            inCorner = false;
        }



        tank_control.MoveVertical(vertical);
        tank_control.MoveHorizontal(horizontal);
    }
}
