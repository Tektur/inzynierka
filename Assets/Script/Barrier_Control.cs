﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Barrier_Control : MonoBehaviour {

	float alive;
	float alive_en;

	void ChangeColor(Color newc)
	{
		Material material = new Material (Shader.Find ("Transparent/Diffuse"));
		material.color = newc;
		GetComponent<Renderer> ().material = material;
	}

	// Use this for initialization
	void Start () {
		alive = 7.0f;
		alive_en = 5.5f;
		Color color = Color.blue;
		color.a = 0.5f;
		ChangeColor (color);
	}  
	
	// Update is called once per frame
	void Update () {
		if (alive > 0)
			alive -= 1.0f * Time.deltaTime;
		else
			Destroy (gameObject);
		if (alive < alive_en) {
			Color color = Color.blue;
			color.a = 1.0f;
			ChangeColor(color);
			GetComponent<BoxCollider> ().enabled = true;
		}

	}

}
