﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "ControlData")]
[System.Serializable]
public class ControlData : ScriptableObject
{
    [SerializeField]
    public List<ArrayWrapper> inputData;

    [SerializeField]
    public List<ArrayWrapper> outputData;

    public ControlData()
    {
        inputData = new List<ArrayWrapper>();
        outputData = new List<ArrayWrapper>();
    }

    public void Copy(ControlData newControlData)
    {
        inputData = new List<ArrayWrapper>(newControlData.inputData);
        outputData = new List<ArrayWrapper> (newControlData.outputData);
    }

    public List<float[]> ToArrayForm(List<ArrayWrapper> list)
    {
        List<float[]> tempList = new List<float[]>();

        //float[] tempArray = new float[list[0].array.Length];

        for(int i = 0; i < list.Count; i++)
        {
            tempList.Add(list[i].array);
        }

        return tempList;
    }
}

[System.Serializable]
public class ArrayWrapper
{
    [SerializeField]
    public float[] array;

    int id = 0;

    public void Add(float newEl)
    {
        if (id < array.Length)
        {
            array[id] = newEl;
            id++;
        }
    }

    public ArrayWrapper(int length)
    {
        id = 0;
        array = new float[length];
    }
}