﻿using UnityEngine;
using System.Collections.Generic;

public class TwoDArrayWrapper
{
    [SerializeField]
    public ArrayWrapper[] twoDArray;

    int id = 0;

    public void Add(ArrayWrapper newEl)
    {
        if (id < twoDArray.Length)
        {
            twoDArray[id] = newEl;
            id++;
        }
    }

    public TwoDArrayWrapper(int length)
    {
        id = 0;
        twoDArray = new ArrayWrapper[length];
    }
};


[CreateAssetMenu(fileName = "NetworkSettings")]
public class NetworkSettings : ScriptableObject
{
    public float eto;
    public List<int> layers;

    public List<ArrayWrapper> weights;
    public List<ArrayWrapper> sizes;
    
    public void SaveWeights(NeuralNetwork net)
    {
        weights = new List<ArrayWrapper>();
        sizes = new List<ArrayWrapper>();

        for (int i = 0; i < net.layers.Length; i++)
        {
            sizes.Add(new ArrayWrapper(2));
            sizes[i].array[0] = net.layers[i].weights.GetLength(0);
            sizes[i].array[1] = net.layers[i].weights.GetLength(1);

            weights.Add(new ArrayWrapper((int) sizes[i].array[0] * (int) sizes[i].array[1]));


            for (int j = 0; j < sizes[i].array[0]; j++)
            {
                for(int k = 0; k < sizes[i].array[1]; k++)
                {
                    weights[i].Add(net.layers[i].weights[j, k]);
                }
            }
        }
    }

    public void LoadWeights(NeuralNetwork net)
    {
        for(int i = 0; i < net.layers.Length; i++)
        {
            for (int j = 0; j < sizes[i].array[0]; j++)
            {
                for (int k = 0; k < sizes[i].array[1]; k++)
                {
                    net.layers[i].weights[j, k] = weights[i].array[(int) (j * sizes[i].array[1] + k)];
                }
            }
        }
    }

    public NetworkSettings()
    {
        weights = new List<ArrayWrapper>();
        sizes = new List<ArrayWrapper>();
    }
    
}