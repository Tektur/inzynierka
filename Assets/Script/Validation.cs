﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Validation
{
    private static System.Random rng = new System.Random();

    public static void Shuffle(List<float[]> inputList, List<float[]> outputList)
    {
        int n = inputList.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            float[] value = inputList[k];
            inputList[k] = inputList[n];
            inputList[n] = value;

            value = outputList[k];
            outputList[k] = outputList[n];
            outputList[n] = value;
        }
    }

    public float ValidateData(NeuralNetwork neuralNetwork, List<float[]> inputData, List<float[]> expectedData, float margError)
    {
        float[] errDiff = new float[inputData[0].Length];
        int correctOutputs = 0;

        if (inputData.Count != expectedData.Count)
        {
            Debug.Log("Invalid number of data samples");
            return 0f;
        }

        for (int i = 0; i < inputData.Count; i++)
        {
            neuralNetwork.FeedForward(inputData[i]);
            for (int j = 0; j < neuralNetwork.Outputs.Length; j++)
            {
                errDiff[j] += Mathf.Abs(neuralNetwork.Outputs[j] - expectedData[i][j]);

                if (Mathf.Abs(neuralNetwork.Outputs[j] - expectedData[i][j]) < margError)
                    correctOutputs++;
            }
                
        }

        for (int i = 0; i < inputData[0].Length; i++)
        {
            errDiff[i] /= inputData.Count;
        }

        float sumErrDiff = 0f;

        for (int i = 0; i < inputData[0].Length; i++)
        {
            sumErrDiff += errDiff[i];
        }

        sumErrDiff /= inputData[0].Length;

        Debug.Log("Correct outputs with the margin of error " + margError + " equals: " + correctOutputs);
        Debug.Log("Average error was " + sumErrDiff);

        return (float)correctOutputs / ((float)inputData.Count * inputData[0].Length);
    }

    public float ValidateTrainTest(NeuralNetwork net, List<float[]> inputData, List<float[]> outputData, int iter, float per, float errMargin)
    {
        int prevAccCount = 0;
        float prevAcc = 0f;
        float acc = 0f;

        Shuffle(inputData, outputData);

        int trainNum = (int) Mathf.Round(inputData.Count * per);
        int testNum = inputData.Count - trainNum;

        if (per == 0)
        {
            trainNum = inputData.Count;
            testNum = 0;
        }

        List<float[]> trainInputData = inputData.GetRange(0, trainNum);
        List<float[]> trainOutputData = outputData.GetRange(0, trainNum);
        List<float[]> testInputData = inputData.GetRange(trainNum, testNum);
        List<float[]> testOutputData = outputData.GetRange(trainNum, testNum);

        for(int i = 0; i < iter/10; i++)
        {
            net.Train(trainInputData, trainOutputData, iter/10);

            if (testNum != 0)
                acc = ValidateData(net, testInputData, testOutputData, errMargin);
            else
                acc = ValidateData(net, trainInputData, trainOutputData, errMargin);

            if (acc < prevAcc)
            {
                prevAccCount++;
                if (prevAccCount > 1)
                {
                    Debug.Log("Przeuczono");
                    return acc;
                }
            }
            else
            {
                prevAccCount = 0;
                prevAcc = acc;
            }
                
        }

        return acc;
    }

    public float KCrossValidation(NeuralNetwork neuralNetwork, int k, List<float[]> inputData, List<float[]> outputData, int iterations)
    {
        Shuffle(inputData, outputData);

        int dataNum = Mathf.FloorToInt(inputData.Count / (float)k);
        //TODO: Get the additional data under control
        int additionData = inputData.Count - (dataNum * k);

        List<List<float[]>> inputDataFolds = new List<List<float[]>>();
        List<List<float[]>> outputDataFolds = new List<List<float[]>>();

        for (int i = 0; i < k; i ++)
        {
            inputDataFolds[i] = inputData.GetRange(i * dataNum, dataNum);
            outputDataFolds[i] = outputData.GetRange(i * dataNum, dataNum);
        }

        List<float[]> inputTrainData = new List<float[]>();
        List<float[]> outputTrainData = new List<float[]>();
        List<float[]> inputTestData = new List<float[]>();
        List<float[]> outputTestData = new List<float[]>();

        float singVal = 0f;
        float avgVal = 0f;

        for(int i = 0; i < k; i ++)
        {
            inputTrainData.Clear();
            outputTrainData.Clear();
            inputTestData.Clear();
            outputTestData.Clear();

            for (int j = 0; i < k; j++)
            {
                if (j == i)
                {
                    inputTestData.AddRange(inputDataFolds[j]);
                    outputTestData.AddRange(outputDataFolds[j]);
                }
                else
                {
                    inputTrainData.AddRange(inputDataFolds[j]);
                    outputTrainData.AddRange(outputDataFolds[j]);
                }
            }

            neuralNetwork.Train(inputTrainData, outputTrainData, iterations);
            singVal = ValidateData(neuralNetwork, inputTestData, outputTestData, 0.5f);

            avgVal += singVal;

        }

        avgVal /= k;

        return avgVal;
    }
}
