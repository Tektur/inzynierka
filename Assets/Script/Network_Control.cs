﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Network_Control : MonoBehaviour {

    public float timeBetween;
    public NetworkSettings networkSettings;

    float horizontal;
    float vertical;
    bool shoot;

    float timer;
    float[] data;

    NeuralNetwork net;
    Tank_Control tankControl;

    public void LoadNet()
    {
        net = new NeuralNetwork(networkSettings.layers.ToArray(), networkSettings.eto);
        networkSettings.LoadWeights(net);

    }

    public void GatherData()
    { 

        for (int i = 0; i < tankControl.sensors.Length; i++)
        {
            data[i] = tankControl.sensors[i].inSight;
        }

    }

    public void MakeDecision()
    {
        GatherData();
        net.FeedForward(data);

        vertical = Mathf.Round(net.Outputs[0]);
        horizontal = Mathf.Round(net.Outputs[1]);
        shoot = net.Outputs[2] > 0f;

        Debug.Log("SHOOT: " + net.Outputs[2]);
    }

    // Use this for initialization
    void Start () {

        tankControl = GetComponent<Tank_Control>();
        data = new float[8];

        LoadNet();
        tankControl.ChangeColor(Color.red);

    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        if(timer > timeBetween)
        {
            timer = 0f;
            MakeDecision();
        }

        tankControl.MoveHorizontal(horizontal);
        tankControl.MoveVertical(vertical);

        if(shoot)
        {
            tankControl.Shoot();
            shoot = false;
        }
	}
}
