﻿using System;
using System.Collections;
using System.Collections.Generic;

public class NeuralNetwork
{

    int[] layer;
    public Layer[] layers;

    public float[] Outputs
    {
        get
        {
            return layers[layers.Length - 1].outputs;
        }
    }

    public NeuralNetwork(int[] layer, float eto)
    {
        this.layer = new int[layer.Length];

        for (int i = 0; i < layer.Length; i++)
            this.layer[i] = layer[i];

        layers = new Layer[layer.Length - 1];

        for (int i = 0; i < layers.Length; i++)
        {
            layers[i] = new Layer(layer[i], layer[i + 1], eto);
        }
    }

    public float[] FeedForward(float[] inputs)
    {
        layers[0].FeedForward(inputs);

        for (int i = 1; i < layers.Length; i++)
        {
            layers[i].FeedForward(layers[i - 1].outputs);
        }

        return layers[layers.Length - 1].outputs;
    }

    public void BackProp(float[] expected)
    {
        layers[layers.Length - 1].BackPropOutput(expected);

        for (int i = layers.Length - 2; i > -1; i--)
        {
            layers[i].BackPropHidden(layers[i + 1].gamma, layers[i + 1].weights);
        }

        for (int i = 0; i < layers.Length; i++)
        {
            layers[i].UpdateWeights();
        }
    }

    public void Train(List<float[]> trainData, List<float[]> expectedData, int iterations)
    {
        if (trainData.Count != expectedData.Count)
        {
            return;
        }

        for (int i = 0; i < iterations; i++)
        {
            for (int j = 0; j < trainData.Count; j++)
            {
                FeedForward(trainData[j]);
                BackProp(expectedData[j]);
            }
        }
    }

    public class Layer
    {

        int numberOfInputs;
        int numberOfOutputs;
        float eto;

        public float[] outputs;
        float[] inputs;
        public float[,] weights;
        float[,] weightsDelta;
        public float[] gamma;
        float[] error;
        static Random random = new Random();

        public Layer(int numberOfInputs, int numberOfOutputs, float eto)
        {
            this.numberOfInputs = numberOfInputs;
            this.numberOfOutputs = numberOfOutputs;
            this.eto = eto;

            outputs = new float[numberOfOutputs];
            inputs = new float[numberOfInputs];
            weights = new float[numberOfOutputs, numberOfInputs];
            weightsDelta = new float[numberOfOutputs, numberOfInputs];
            gamma = new float[numberOfOutputs];
            error = new float[numberOfOutputs];

            InitializeWeights();
        }


        public void InitializeWeights()
        {
            for (int i = 0; i < numberOfOutputs; i++)
            {
                for (int j = 0; j < numberOfInputs; j++)
                {
                    weights[i, j] = (float)random.NextDouble() - 0.5f;
                }
            }
        }

        public float[] FeedForward(float[] inputs)
        {
            this.inputs = inputs;

            for (int i = 0; i < numberOfOutputs; i++)
            {
                outputs[i] = 0;

                for (int j = 0; j < numberOfInputs; j++)
                {
                    outputs[i] += inputs[j] * weights[i, j];
                }

                //TODO: ACTIVATION FUNCTION HERE
                outputs[i] = (float)Math.Tanh(outputs[i]);
                //outputs[i] = (float) 1f / (float) (1f + Math.Exp(-outputs[i]));
            }

            return outputs;
        }

        public void BackPropOutput(float[] expected)
        {
            for (int i = 0; i < numberOfOutputs; i++)
                error[i] = outputs[i] - expected[i];

            for (int i = 0; i < numberOfOutputs; i++)
                gamma[i] = error[i] * TanHDerr(outputs[i]);

            for (int i = 0; i < numberOfOutputs; i++)
            {
                for (int j = 0; j < numberOfInputs; j++)
                {
                    weightsDelta[i, j] = gamma[i] * inputs[j];
                }
            }
        }

        public void BackPropHidden(float[] gammaForward, float[,] weightsForward)
        {
            for (int i = 0; i < numberOfOutputs; i++)
            {
                gamma[i] = 0;

                for (int j = 0; j < gammaForward.Length; j++)
                {
                    gamma[i] += gammaForward[j] * weightsForward[j, i];
                }

                gamma[i] *= TanHDerr(outputs[i]);
            }

            for (int i = 0; i < numberOfOutputs; i++)
            {
                for (int j = 0; j < numberOfInputs; j++)
                {
                    weightsDelta[i, j] = gamma[i] * inputs[j];
                }
            }
        }

        public void UpdateWeights()
        {
            for (int i = 0; i < numberOfOutputs; i++)
            {
                for (int j = 0; j < numberOfInputs; j++)
                {
                    weights[i, j] -= weightsDelta[i, j] * eto;
                }
            }
        }

        public float TanHDerr(float value)
        {
            return 1 - (value * value);
        }

    }
}
