﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour {

    public GameObject target;
    public float inSight;

	// Use this for initialization
	void Start () {
        inSight = -1f;
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == target)
        {
            inSight = 1f;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == target)
        {
            inSight = -1f;
        }
    }
}
