﻿using UnityEngine;
using System.Collections;

public class AI_Control : MonoBehaviour {

	Tank_Control tank_control;
	bool Run;

	// Use this for initialization
	void Start () {
		tank_control = GetComponent<Tank_Control> ();
		tank_control.enemy = GameObject.FindWithTag ("Player");

        tank_control.ChangeColor(Color.blue);
    }
		
	// Update is called once per frame
	void Update () {
		if (tank_control.GetEnemyDistance (transform.position) > 4.0f) {
			Vector3 temp_pos = tank_control.AfterMove (1);

			float temp_rot = tank_control.GetEnemyDiffAngle (transform.forward);
			if (temp_rot > tank_control.GetEnemyDiffAngle (tank_control.AfterRotate (1)))
				tank_control.MoveHorizontal (-1);
			else
				tank_control.MoveHorizontal (1);


			if (tank_control.GetEnemyDistance (transform.position) > tank_control.GetEnemyDistance (temp_pos))
				tank_control.MoveVertical (1);
			else
				tank_control.MoveVertical (-1);
		} else if (tank_control.GetEnemyDistance (transform.position) < 3.0f) {
			Vector3 temp_pos = tank_control.AfterMove (1);

			float temp_rot = tank_control.GetEnemyDiffAngle (transform.forward);
			if (temp_rot > tank_control.GetEnemyDiffAngle (tank_control.AfterRotate (1)))
				tank_control.MoveHorizontal (1);
			else
				tank_control.MoveHorizontal (-1);


			if (tank_control.GetEnemyDistance (transform.position) < tank_control.GetEnemyDistance (temp_pos))
				tank_control.MoveVertical (1);
			else
				tank_control.MoveVertical (-1);
		} else if (tank_control.GetEnemyDiffAngle (transform.forward) > 0.3f) {
			float temp_rot = tank_control.GetEnemyDiffAngle (transform.forward);
			if (temp_rot > tank_control.GetEnemyDiffAngle (tank_control.AfterRotate (1)))
				tank_control.MoveHorizontal (1);
			else
				tank_control.MoveHorizontal (-1);
		} else {
			tank_control.MoveVertical (0.30f);
			tank_control.MoveHorizontal (-0.35f);
		}

		Vector3 rand = new Vector3 (Random.Range (-0.3f, 0.3f), 0, Random.Range (-0.3f, 0.3f));
		//if(tank_control.CheckVisible(tank_control.enemy.transform.position)) tank_control.Shoot ();
	}
}
