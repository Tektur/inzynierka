﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameController_Control : MonoBehaviour {

	public GameObject barrier;
	float time;
	float time_max;

	void CreateBarrier(int temp)
	{
		if (temp == 1) {
			Instantiate (barrier, new Vector3(0.0f, 0.3f, 0.0f), Quaternion.Euler(0,0,0));
		} else if (temp == 2) {
			Instantiate (barrier, new Vector3(0.0f, 0.3f, 0.0f), Quaternion.Euler(0,90,0));
		}
	}

	// Use this for initialization
	void Start () {
		time_max = 9.0f;
		time = 2.0f;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (time < 0.0f) {
			time = time_max;

			int temp = Random.Range (1, 3);
			//CreateBarrier (temp);
		}


		time -= 1.0f * Time.deltaTime;
	}

	public void ResetGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void QuitGame()
	{
		Application.Quit();
	}
}
