﻿using UnityEngine;
using System.Collections;

public class Cannon_Control : MonoBehaviour {

	public float speed;
	public GameObject bullet;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Ray target = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast (target, out hit)) {
			Vector3 targetDir = hit.point - transform.position;
			Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, 100.0f, 0.0f);
			newDir.y = 0.0f;
			transform.rotation = Quaternion.LookRotation (newDir);
		}

		if(Input.GetButtonUp("Fire1"))
		{
			GameObject bull = Instantiate(bullet, transform.position + 0.5f * transform.forward, transform.rotation) as GameObject;
			bull.transform.forward = transform.forward;
		}
	}
}
