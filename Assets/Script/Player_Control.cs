﻿using UnityEngine;
using System.Collections;

public class Player_Control : MonoBehaviour {

	Tank_Control tank_control;

	// Use this for initialization
	void Start () {
		tank_control = GetComponent<Tank_Control> ();
		tank_control.enemy = GameObject.FindWithTag ("Enemy");

        tank_control.ChangeColor(Color.green);
	}
	
	// Update is called once per frame
	void Update () {
		tank_control.MoveVertical (Input.GetAxis ("Vertical"));
		tank_control.MoveHorizontal (Input.GetAxis ("Horizontal"));
		if (Input.GetButton ("Jump")) {
			tank_control.Shoot ();
		}
	}
}
