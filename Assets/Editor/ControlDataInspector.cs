﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;


//[CustomEditor(typeof(ControlData))]
public class ControlDataInspector : Editor
{
    ControlData controlData;

    int dataListCount;

    public void OnEnable()
    {
        controlData = (ControlData)target;
        /*
        if (controlData.inputData == null)
            controlData.inputData = new List<float[]>();

        if (controlData.outputData == null)
            controlData.outputData = new List<float[]>();
         
        dataListCount = controlData.inputData.Count;
        */
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        GUILayout.BeginVertical();
        
        if(GUILayout.Button("Save"))
        {
            /*
            orgControlData.Copy(editorControlData);
            EditorUtility.SetDirty(orgControlData);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            */
        }
        

        GUILayout.Space(20);

        GUILayout.Label("Input");

        GUILayout.BeginHorizontal();
        GUILayout.Label("Elements: ");
        //dataListCount = EditorGUILayout.IntField(controlData.inputData.Count);
        GUILayout.Label(controlData.inputData.Count.ToString());
        GUILayout.EndHorizontal();
        /*
        while (dataListCount > controlData.inputData.Count)
        {
            controlData.inputData.Add(new float[2]);
        }

        while (dataListCount < controlData.inputData.Count)
        {
            controlData.inputData.RemoveAt(controlData.inputData.Count - 1);
        }
        */
        for (int i = 0; i < controlData.inputData.Count; i++)
        {
            GUILayout.BeginHorizontal();
            for(int j = 0; j < controlData.inputData[i].array.Length; j++)
            {
                //controlData.inputData[i][j] = EditorGUILayout.FloatField(controlData.inputData[i][j]);
                GUILayout.Label(controlData.inputData[i].array[j].ToString());
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(10);
        
        GUILayout.Label("Output");

        GUILayout.BeginHorizontal();
        GUILayout.Label("Elements: ");
        //dataListCount = EditorGUILayout.IntField(controlData.outputData.Count);
        GUILayout.Label(controlData.outputData.Count.ToString());
        GUILayout.EndHorizontal();

        /*
        while (dataListCount > controlData.outputData.Count)
        {
            controlData.outputData.Add(new float[1]);
        }

        while (dataListCount < controlData.outputData.Count)
        {
            controlData.outputData.RemoveAt(controlData.outputData.Count - 1);
        }
        */

        for (int i = 0; i < controlData.outputData.Count; i++)
        {
            GUILayout.BeginHorizontal();
            for (int j = 0; j < controlData.outputData[i].array.Length; j++)
            {
                //controlData.outputData[i][j] = EditorGUILayout.FloatField(controlData.outputData[i][j]);
                GUILayout.Label(controlData.outputData[i].array[j].ToString());
            }
            GUILayout.EndHorizontal();
        }
        
        GUILayout.EndVertical();

        if(EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();
        }
    }
}
