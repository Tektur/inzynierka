﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

public class NetworkConfiguration : EditorWindow
{
    NetworkSettings networkSettings;
    ControlData controlData;
    NeuralNetwork net;
    Validation validation;

    NetworkConfigTabs tab;

    //Validate Data variables
    float validatePercent;
    int validateIter;
    float validateOutput;
    float errMargin;

    string output;

    [MenuItem("Network/NetworkConfiguration")]
    public static void ShowWindow()
    {
        GetWindow<NetworkConfiguration>(false, "NetworkConfiguration", true);
    }

    void OnEnable()
    {
        validation = new Validation();

        if (EditorPrefs.HasKey("ObjectNetworkPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectNetworkPath");
            networkSettings = AssetDatabase.LoadAssetAtPath(objectPath, typeof(NetworkSettings)) as NetworkSettings;

            net = new NeuralNetwork(networkSettings.layers.ToArray(), networkSettings.eto);
        }

        if (EditorPrefs.HasKey("ObjectDataPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectDataPath");
            controlData = AssetDatabase.LoadAssetAtPath(objectPath, typeof(ControlData)) as ControlData;
        }
    }

    private void OnGUI()
    {
        //Top Menu
        GUILayout.BeginVertical();

        //Main Network Options
        GUILayout.BeginHorizontal();
        GUILayout.Label("Network Settings", EditorStyles.boldLabel);
        if (networkSettings != null)
        {
            if (GUILayout.Button("Show Network Settings"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = networkSettings;
            }
        }

        if (GUILayout.Button("Open Network Settings"))
        {
            OpenNetworkData();
        }

        if (GUILayout.Button("Save Network Settings"))
        {
            EditorUtility.SetDirty(networkSettings);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        GUILayout.EndHorizontal();
        //Main Network Options - END

        //Main Data Options
        GUILayout.BeginHorizontal();
        GUILayout.Label("Control Data", EditorStyles.boldLabel);
        if (controlData != null)
        {
            if (GUILayout.Button("Show Control Data"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = controlData;
            }
        }

        if (GUILayout.Button("Open Control Data"))
        {
            OpenControlData();
        }

        GUILayout.EndHorizontal();
        //Main Data Options - END

        GUILayout.Space(10);

        //Main Tabs
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Change Network Layers"))
        {
            tab = NetworkConfigTabs.ChangeNetworkLayers;
        }

        if (GUILayout.Button("Validate Data"))
        {
            tab = NetworkConfigTabs.ValidateData;
        }

        GUILayout.EndHorizontal();
        //Main Tabs - END

        GUILayout.EndVertical();
        //Top Menu - END

        GUILayout.Space(30);


        //Main Tab
        GUILayout.BeginVertical();

        if(tab == NetworkConfigTabs.ChangeNetworkLayers)
        {
            if(GUILayout.Button("Load from settings again"))
            {
                net = new NeuralNetwork(networkSettings.layers.ToArray(), networkSettings.eto);
            }
        }

        if(tab == NetworkConfigTabs.ValidateData)
        {
            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Train - Test Percent");
            validatePercent = EditorGUILayout.FloatField(validatePercent);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Number of iterations");
            validateIter = EditorGUILayout.IntField(validateIter);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Error margin");
            errMargin = EditorGUILayout.FloatField(errMargin);
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Train and test with current loaded data"))
            {
                net = new NeuralNetwork(networkSettings.layers.ToArray(), networkSettings.eto);
                validateOutput = validation.ValidateTrainTest(net, controlData.ToArrayForm(controlData.inputData), controlData.ToArrayForm(controlData.outputData), validateIter, validatePercent, errMargin);
                output = "Validation accuracy: " + validateOutput;
            }

            if (GUILayout.Button("Save weights to settings"))
            {
                networkSettings.SaveWeights(net);
            }

            GUILayout.EndVertical();
        }

        GUILayout.EndVertical();
        //Main Tab - END

        //Output text

        GUILayout.Space(20);

        GUILayout.Label(output);
    }

    private void OpenNetworkData()
    {
        string absPath = EditorUtility.OpenFilePanel("Select the network data", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            networkSettings = AssetDatabase.LoadAssetAtPath(relPath, typeof(NetworkSettings)) as NetworkSettings;
            if (networkSettings == null)
                networkSettings = new NetworkSettings();
            if (networkSettings)
            {
                EditorPrefs.SetString("ObjectNetworkPath", relPath);
            }
        }
    }

    private void OpenControlData()
    {
        string absPath = EditorUtility.OpenFilePanel("Select the control data", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            controlData = AssetDatabase.LoadAssetAtPath(relPath, typeof(ControlData)) as ControlData;
            if (controlData == null)
                controlData = new ControlData();
            if (controlData)
            {
                EditorPrefs.SetString("ObjectDataPath", relPath);
            }
        }
    }

}
